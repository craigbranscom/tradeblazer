/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blazeengine;

import java.util.ArrayList;
import java.util.Stack;

/**
 * @author Stephen Craig Branscom
 * @version 1.0 February 9th, 2018
 */
public class Exchange implements ExchangeInterface{
    public String exchangeName;
    
    private ArrayList<Pair> tradingPairs;
    private int pairCount;
    
    /**
     * Exchange Constructor
     */
    public Exchange(String name) {
        exchangeName = name;
        tradingPairs = new ArrayList<Pair>();
        pairCount = 0;
    }
    
    public void createTradingPair(Cryptocurrency base, Cryptocurrency minor, double rate, double volume) {
        tradingPairs.add(new Pair(exchangeName, base, minor, rate, volume));
    }
    
    
    /**
     * 
     * @param c
     * @return 
     */
    public Stack filterPairsByCurrency(Cryptocurrency c) {
        Stack filtered = new Stack();
        
        for (int i = 0; i < tradingPairs.size(); i++) {
            if (c.equals(tradingPairs.get(i).getBaseTicker())) { //filters by base ticker
                filtered.push(tradingPairs.get(i));
            }
            else if (c.equals(tradingPairs.get(i).getMinorTicker())) { //filters by minor ticker
                filtered.push(tradingPairs.get(i));
            }
        }
        
        return filtered;
    }
    
    public void summarizeExchange() {
        System.out.println("" + exchangeName + ":");
        System.out.println("---------------------");
        
        for (int i = 0; i < tradingPairs.size(); i++) {
            System.out.println("" + tradingPairs.get(i).getBaseTicker().toString() + "/" + tradingPairs.get(i).getMinorTicker().toString());
            System.out.println("    Rate: " + tradingPairs.get(i).getExchangeRate());
            System.out.println("    Volume: " + tradingPairs.get(i).getVolume());
        }
    }
    
    public void countPairs() {
        pairCount = tradingPairs.size();
    }
}