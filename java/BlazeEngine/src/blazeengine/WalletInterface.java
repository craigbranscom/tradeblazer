/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blazeengine;

/**
 *
 * @author Stephen Craig Branscom
 * @version 1.0 February 9th, 2018
 */
public interface WalletInterface {
    public void setCurrency(Cryptocurrency c);
    public Cryptocurrency getCurrency();
    public void setQuantity(double quan);
    public double getQuantity();
    public void summarizeWallet();
}