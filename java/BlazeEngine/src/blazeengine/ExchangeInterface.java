/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blazeengine;

/**
 * @author Stephen Craig Branscom
 * @version 1.0 February 9th, 2018
 */
public interface ExchangeInterface {
    public void createTradingPair(Cryptocurrency base, Cryptocurrency minor, double rate, double volume);
    public void summarizeExchange();
}