/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blazeengine;

/**
 *
 * @author Stephen Craig Branscom
 * @version 1.0 February 9th, 2018
 */
public class Wallet implements WalletInterface{
    private Cryptocurrency currency;
    private double quantity;
    //private String countervalueAsset;
    //private double countervalue;
    
    /**
     * Wallet Constructor
     * @param c of enumerated type Cryptocurrency
     * @param quan of type double
     */
    public Wallet(Cryptocurrency c, double quan) {
        currency = c;
        quantity = quan;
    }
    
    /**
     * 
     * @param c of enumerated type Cryptocurrency
     */
    public void setCurrency(Cryptocurrency c) {
        currency = c;
    }
    
    public Cryptocurrency getCurrency() {
        return currency;
    }
    
    public void setQuantity(double quan) {
        quantity = quan;
    }
    
    public double getQuantity() {
        return quantity;
    }
    
    public void summarizeWallet() {
        System.out.println("Wallet Summary:");
        System.out.println("" + quantity + " " + currency.toString());
    }
}