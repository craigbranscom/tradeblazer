/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blazeengine;

/**
 * @author Stephen Craig Branscom
 * @version 1.0 February 9th, 2018
 */
public class Pair implements PairInterface{
    private String exchange;
    private Cryptocurrency baseTicker;
    private Cryptocurrency minorTicker;
    private double exchangeRate;
    private double volume;
    
    /**
     * TradingPair Constructor
     */
    public Pair(String exch, Cryptocurrency base, Cryptocurrency minor, double rate, double vol) {
        exchange = exch;
        baseTicker = base;
        minorTicker = minor;
        exchangeRate = rate;
        volume = vol;
    }
    
    public String getExchange() {
        return exchange;
    }
    
    public Cryptocurrency getBaseTicker() {
        return baseTicker;
    }
    
    public Cryptocurrency getMinorTicker() {
        return minorTicker;
    }
    
    public double getExchangeRate() {
        return exchangeRate;
    }
    
    public double getVolume() {
        return volume;
    }
    
    public String getPairText() {
        return "" + baseTicker + "/" + minorTicker;
    }
    
    public void summarizePair() {
        
    }
}