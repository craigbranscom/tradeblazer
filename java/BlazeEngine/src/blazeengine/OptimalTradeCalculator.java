/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blazeengine;

import static blazeengine.Cryptocurrency.*;
import java.util.Stack;

/**
 *
 * @author Stephen Craig Branscom
 * @version 1.0 February 9th, 2018
 */
public class OptimalTradeCalculator {
    
    /**
     * EngineController Constructor
     */
    public OptimalTradeCalculator() {
        
    }
    
    /**
     * Executes a trade.
     * @param p 
     * @param w
     */
    public void trade(Pair p, Wallet w) {
        if (w.getCurrency().equals(p.getBaseTicker())) {
            w.setQuantity(w.getQuantity() / p.getExchangeRate());
            w.setCurrency(p.getMinorTicker());
            System.out.println("Base Trade Executed");
            System.out.println("");
        }
        else {
            w.setQuantity(w.getQuantity() * p.getExchangeRate());
            w.setCurrency(p.getBaseTicker());
            System.out.println("Minor Trade Executed");
            System.out.println("");
        }
        w.summarizeWallet();
    }
    
    public void transfer(Pair p, Wallet w) {
        
    }
    
    
    /**
     * 
     */
    public void chooseNextDecision() {
        
    }
    
    /**
     * Summarizes a Blaze on the command line.
     */
    public void summarizeBlaze(Stack s, Wallet w) {
        System.out.println("Blaze Summary:");
        System.out.print("Wallet");
        
        while (s.empty() == false) {
            Pair p = (Pair)s.pop();
            System.out.print(" -> " + p.getPairText());
        }
        
        System.out.print(" -> Wallet");
        System.out.println("");
    }
    
    /**
     * Builds a Stack trace of optimal trade route.
     * @return Stack of Pairs representing optimal arbitrage chain
     */
    public Stack blaze(Wallet w) {
        Stack trail = new Stack();
        w.summarizeWallet();
        
        System.out.println("");
        
        //testing
        trade(new Pair("Poloniex", BTC, ETH, 0.07939, 5.1), w);     
        trail.add(new Pair("Poloniex", BTC, ETH, 0.065, 5.1));
        
        trade(new Pair("Bittrex", BTC, ETH, 0.0828, 9.1), w);
        trail.add(new Pair("Bittrex", BTC, ETH, 0.1, 9.1));
        
        System.out.println("");
        
        summarizeBlaze(trail, w);
        
        System.out.println("");
        
        w.summarizeWallet();
        return trail;
    }
}