/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package blazeengine;

import static blazeengine.Cryptocurrency.*;
import java.util.ArrayList;

/**
 *
 * @author Stephen Craig Branscom
 * @version 1.0 February 14th, 2018
 */
public class MarketState {
    private ArrayList<Exchange> state;
    
    /**
     *  MarketState Constructor
     */
    public MarketState() {
        Wallet wallet = new Wallet(BTC, 1.1334354418431218);
        
        Exchange Px = new Exchange("Poloniex");
        Px.createTradingPair(BTC, ETH, 0.0792, 2533.1);
        Px.createTradingPair(BTC, LTC, 0.018, 3.2);
        Px.createTradingPair(BTC, ETN, 0.001, 1.1);
        Px.summarizeExchange();
        
        Exchange Bx = new Exchange("Bittrex");
        Bx.createTradingPair(BTC, ETH, 0.0791, 25225.8);
        Bx.createTradingPair(BTC, LTC, 0.02, 4.6);
        Bx.createTradingPair(BTC, ETN, 0.0014, 2.1);
        Bx.summarizeExchange();
        
        OptimalTradeCalculator calc = new OptimalTradeCalculator();
        calc.blaze(wallet);
    }
}