import static net.sourceforge.jwebunit.junit.JWebUnit.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class TradeBlazerTest.
 *
 * @author  Vance Field
 * @version 25-Jan-2018
 */
public class TradeBlazerTest
{
    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void prepare()
    {
        setBaseUrl("http://tradeblazer.io/tradeblazer/");
        beginAt("");
    }

    @Test
    public void test1(){
        assertTitleEquals("Tradeblazer Login");
    }
    
    
    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void close()
    {
        closeBrowser();
    }
}
