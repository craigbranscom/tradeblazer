# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary of our development workflow
* Version 30-Nov-2017

### How do I get set up? ###

* Summary of set up
Any IDE or Text Editor will suffice for development
Install Git
To open Git, simply right click inside of a directory and click Git Bash Here
It is better to learn Git commands and use Git Bash than using Git GUI
Open Git and configure your email and name. Use the email you use for Bitbucket. Enter these commands:
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
Create your development directory
Create a new folder for development on your local machine (Ex: "G:\School\ITEC 471\dev")
Browse inside of your new development folder and open Git Bash 
Type "git clone https://your-username@bitbucket.org/craigbranscom/tradeblazer.git". This will clone our repository inside of your development folder.
You should now have the folder "tradeblazer" inside of your development folder
"cd" into that directory in Git Bash
Type "git status" and make sure that you are on the master branch
Type "git fetch" to get an updated list of the repositories branches
Your development directory is now set up
Questions about Git? http://guides.beanstalkapp.com/version-control/common-git-commands.html


* Database configuration
ssh into "tradeblazer.io" with putty
 usernames: 
  kbutler
  ddavis
  cfrench
  vfield
 password:
  CryptoFive2017
run mysql -u <username> -p
 password:
  Fireb@ll99703 
You are now on mysql
To change to our database, type "use tradeblazer;"
You are now inside of the database

* How to run tests
To test on the server:
ssh and login to the server 
run the commands:
 cd /
 cd var
 cd www
 cd html
 cd tradeblazer
You are now inside of the repository on the server
You can use git to fetch and checkout the branch you need to test
You can use vim to edit code on the server 

* Deployment instructions

Working a Task
	1. Create a new task on JIRA (if there isn't already one)
		Make sure the task has reasonable requirements. Break the task up into separate tasks if it can be done
		Include a link to the Bitbucket branch that you are working on for that task
		Make sure to put a time estimate
		Assign the task
		Start the task
		Don't forget to log your hours and close the task when you are done. If you are committing/pushing to bitbucket, then you should probably be logging your work
	2. Create a new branch with git or Bitbucket for development on the task 
		Git
 			Open your development directory on your local machine (...\dev\tradeblazer)
 			Open Git Bash inside of the tradeblazer folder
 			Type "git branch" to make sure that you are on the master branch. If you aren't, type "git checkout master" to switch.
 			Type "git checkout -b name-of-new-branch". This creates the new branch off of master. The name of the branch should correspond with the task name. It doesn't have to be exactly the same, but similar enough to recognize.
 			Type "git branch" to make sure you are on the new branch. If you aren't, type "git checkout name-of-new-branch" to switch.
 			You are now on your new branch.
 		Bitbucket (recommended)
			Open our Bitbucket repository in your browser, and navigate to the Branches page. (https://bitbucket.org/craigbranscom/tradeblazer/branches/)
			Click "Create branch" in the top right.
			Make sure you Branch from Master.
			Enter a branch name. It should correspond with the task name.
			Click create.
			Open your development directory on your local machine (...\dev\tradeblazer).
			Open Git Bash inside of the tradeblazer folder.
			Type "git fetch". You should see your new branch listed.
			Type "git checkout name-of-new-branch" to switch to your new branch.
			You are now on your new branch.
	3. Develop
			Remember to update the @version and @author tags in each file you work on
			Please remember to comment
			When you reach a milestone in your development, such as implementing a new button, you should push your changes.
				Navigate to your development directory on your local machine.
				Open Git Bash
				Check to make sure that you are on the right branch! (git branch)
				Type "git add ." This adds your /tradeblazer directory to the push. 
				Type "git commit" This will open vim for you to leave a commit message. Keep it short. Hit "i" to insert, type your message, then "esc" to stop typing, and ":wq" to save and close vim.
				Type "git push" to push your changes to your branch.
			Continue doing this until your task has been completed.
			If at any point you encounter a bug or run into a situation where you are working outside the scope of the current task, open JIRA and create a new bug/task for it
	4. Merge your work
			Once you are finished with your programming task, you can then merge your work into the master branch.
			To do this, open our Bitbucket repository in your browser and navigate to the Pull Requests page. (https://bitbucket.org/craigbranscom/tradeblazer/pull-requests/)
			Click Create a Pull Request.
			Select your branch, and make sure you select master as the branch to merge into.
			Title the merge. For naming convention, type "name-of-task Merge".
			Type up a description of everything you have added/removed/changed on your branch.
			Add all of us (Vance Field, Khalil Butler, Davon Davis, Craig Branscom, Cody Allen French) as Reviewers so that we can do a Peer Review of your code. In a code review., a developer can deny your merge due to bugs in your code, poor formatting, etc... You should always thoroughly review someone's code during a code review. This is the part of development where it is the easiest to spot and stop bugs from persisting!
			Once the code review is complete and all developers approve, the branch will be merged into master
	5. Log your work on JIRA and close the task

### Contribution guidelines ###

* Writing tests
TBD

* Code review
Code reviews are done through bitbucket by creating a pull request
Keep an eye on bitbucket and discord to check for open pull requests
You should try to review and complete a code review as soon as you are made aware of it so that the master branch can be kept as up-to-date as possible

* Other guidelines
Please don't write code that is outside the scope of your working task

### Who do I talk to? ###

@everyone