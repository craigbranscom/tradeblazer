<!doctype html>

<!-- 
	dashboard.php

	@author Khalil Butler
	@author Vance Field
	@version 27-Feb-2018
-->
      
<?php
   include 'connection.php';

   session_start();
   $email = $_SESSION['email'];
   
   
?>

<html lang="en">
  <head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="icon" href="resources/CompassLogo.png">
	
    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">	
    <link href="https://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/dashboard.css" rel="stylesheet">
  </head>
	
	<!-- Implements the navbar and its components -->
	<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">
	<img src="resources/CompassLogo.png" width="20" height="20" class="d-inline-block align-top" alt="">
	TradeBlazer
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="dashboard.php">Dashboard<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="cryptocurrencies.php">Cryptocurrencies<span class="sr-only">(current)</span></a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="#">Trade Routes<span class="sr-only">(current)</span></a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="#">Walkthroughs<span class="sr-only">(current)</span></a>
      </li>
    </ul>
	<ul class="navbar-nav">
	<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Account
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="my-profile.php">My Profile</a>
        </div>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="sign-out-handler.php">Sign out<span class="sr-only">(current)</span></a>
      </li>
	 </ul>
  </div>
</nav>
<br>
<br>

		<!-- Implements graph menu bar and its components -->  
        <main role="main" class="mr-sm-auto ml-sm-auto col-md-11 pt-3 px-4">
		<br>
		
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
			<!-- Dark mode <font color="white">Coin Comparison</font> -->
			<h1 class="h2"><font color="black">Coin Comparison</font></h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-warning">Share</button>
              </div>
              <button class="btn btn-sm btn-warning dropdown-toggle">
                This week
              </button>
            </div>
        </div>
		  
		
		
		<!-- implements sidebar and graph-->
		<div class="row">
		
			<!-- sidebar -->
			<div class="sidebar-sticky">				
				  	  
				<!-- list of crypto -->		
				<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					<h2>Coin Pair Selection</h2>
					<br>
					<a class="nav-link-c active text-dark" id="v-pills-btc-eth-tab" data-toggle="pill" href="#v-pills-btc-eth" role="tab" onclick="ethClick()">BTC/ETH</a>
					<br>
					<a class="nav-link-c text-dark" id="v-pills-btc-ltc-tab" data-toggle="pill" href="#v-pills-btc-ltc" role="tab" onclick="ltcClick()">BTC/LTC</a>
					<br>
					<a class="nav-link-c text-dark" id="v-pills-btc-bch-tab" data-toggle="pill" href="#v-pills-btc-bch" role="tab" onclick="bchClick()">BTC/BCH</a>
					<br>
				</div>
			</div>
		
		
			<!-- graph panel -->
			<div class="tab-content" id="v-pills-tabContent" >
		
				<?php						
					$coin_data_query = "SELECT * FROM BTC_ETH WHERE Exchange='Bittrex' ORDER BY Time desc";
					$coin_data_result = mysqli_query($conn, $coin_data_query);				
				?>
			
				
				<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
				<script type='text/javascript'>
				
					google.charts.load('current', {'packages':['corechart']});
					google.charts.setOnLoadCallback(drawChartETH_Bittrex);
					google.charts.setOnLoadCallback(drawChartETH_Coinsquare);
					
					  /**
					   * Draws the chart with coin/pair BTC/ETH from exchange Bittrex
					   */
					  function drawChartETH_Bittrex() {
						var data = new google.visualization.DataTable();
						data.addColumn('datetime', 'Time');
						data.addColumn('number', 'Rate');
						
						var options = {
							title:'Bitcoin / Ethereum from Bittrex',
							displayZoomButtons:true,
							displayAnnotations: true,
							scaleType: 'allmaximized',
							legendPosition: 'newRow',
							thickness: 5,	
							colors: ['#ffa500','#ffa500'],
							width:1000,
							height:400,
							//hAxis: {
							//	maxValue: new Date(2018, 2, 28)
							//}
						};
						
						data.addRows([
							  
							 <?php  
								
								$first_row = 1;
								$num_rows = mysqli_num_rows($coin_data_result);
				  
								while ($row = mysqli_fetch_assoc($coin_data_result)) {
									$time = $row['Time'];
									$price = $row['Rate'];
									
									if  ($first_row != $num_rows) {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price],";
									}
									else {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price]";
									}					
									
									$num_rows--;
								}
							?>							
						/*						
						  Test Data:							
						  [new Date(2008, 1 ,1), 30000],
						  [new Date(2008, 1 ,2), 14045],
						  [new Date(2008, 1 ,3), 55022],						 
						 */ 						
						]);

						var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
						chart.draw(data, options);
					  }

					  /**
					   * Draws the chart with coin/pair BTC/ETH from exchange Coinsquare
					   */
					  function drawChartETH_Coinsquare() {
						  
						<?php						
							$coin_data_query = "SELECT * FROM BTC_ETH WHERE Exchange='Coinsquare' ORDER BY Time desc";
							$coin_data_result = mysqli_query($conn, $coin_data_query);				
						?>
						var data = new google.visualization.DataTable();
						data.addColumn('datetime', 'Time');
						data.addColumn('number', 'Rate');
						
						var options = {
							title:'Bitcoin / Ethereum from Coinsquare',
							displayZoomButtons:true,
							displayAnnotations: true,
							scaleType: 'allmaximized',
							legendPosition: 'newRow',
							thickness: 5,	
							colors: ['#ffa500','#ffa500'],
							width:1000,
							height:400
						};
						
						data.addRows([
							  
							 <?php  
								
								$first_row = 1;
								$num_rows = mysqli_num_rows($coin_data_result);
				  
								while ($row = mysqli_fetch_assoc($coin_data_result)) {
									$time = $row['Time'];
									$price = $row['Rate'];
									
									if  ($first_row != $num_rows) {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price],";
									}
									else {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price]";
									}							
									
									$num_rows--;
								}
							?>							
						/*						
						  Test Data:							
						  [new Date(2008, 1 ,1), 30000],
						  [new Date(2008, 1 ,2), 14045],
						  [new Date(2008, 1 ,3), 55022],						 
						 */ 						
						]);

						var chart = new google.visualization.AreaChart(document.getElementById('chart_div2'));
						chart.draw(data, options);
					  }
					  
					  
					  
					  /**
					   * Draws the chart with coin/pair BTC/LTC from exchange Bittrex
					   */
					  function drawChartLTC_Bittrex() {

						<?php						
							$coin_data_query = "SELECT * FROM BTC_LTC WHERE Exchange='Bittrex' ORDER BY Time desc";
							$coin_data_result = mysqli_query($conn, $coin_data_query);				
						?>

						var data = new google.visualization.DataTable();
						data.addColumn('datetime', 'Time');
						data.addColumn('number', 'Rate');
						
						var options = {
							title:'Bitcoin / Litecoin from Bittrex',
							displayZoomButtons:true,
							displayAnnotations: true,
							scaleType: 'allmaximized',
							legendPosition: 'newRow',
							thickness: 5,	
							colors: ['#ffa500','#ffa500'],
							width:1000,
							height:400
						};
						
						data.addRows([
							  
							 <?php  
								
								$first_row = 1;
								$num_rows = mysqli_num_rows($coin_data_result);
				  
								while ($row = mysqli_fetch_assoc($coin_data_result)) {
									$time = $row['Time'];
									$price = $row['Rate'];
									
									if  ($first_row != $num_rows) {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price],";
									}
									else {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price]";
									}							
									
									$num_rows--;
								}
							?>
				
						]);

						var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
						chart.draw(data, options);
					  }
					  
					  /**
					   * Draws the chart with coin/pair BTC/LTC from exchange Coinsquare
					   */
					  function drawChartLTC_Coinsquare() {

						<?php						
							$coin_data_query = "SELECT * FROM BTC_LTC WHERE Exchange='Coinsquare' ORDER BY Time desc";
							$coin_data_result = mysqli_query($conn, $coin_data_query);				
						?>

						var data = new google.visualization.DataTable();
						data.addColumn('datetime', 'Time');
						data.addColumn('number', 'Rate');
						
						var options = {
							title:'Bitcoin / Litecoin from Coinsquare',
							displayZoomButtons:true,
							displayAnnotations: true,
							scaleType: 'allmaximized',
							legendPosition: 'newRow',
							thickness: 5,	
							colors: ['#ffa500','#ffa500'],
							width:1000,
							height:400
						};
						
						data.addRows([
							  
							 <?php  
								
								$first_row = 1;
								$num_rows = mysqli_num_rows($coin_data_result);
				  
								while ($row = mysqli_fetch_assoc($coin_data_result)) {
									$time = $row['Time'];
									$price = $row['Rate'];
									
									if  ($first_row != $num_rows) {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price],";
									}
									else {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price]";
									}							
									
									$num_rows--;
								}
							?>
				
						]);

						var chart = new google.visualization.AreaChart(document.getElementById('chart_div2'));
						chart.draw(data, options);
					  }
					  
					  
					  /**
					   * Draws the chart with coin/pair BTC/BCH from exchange Bittrex
					   */
					  function drawChartBCH_Bittrex() {

					  <?php						
							$coin_data_query = "SELECT * FROM BTC_BCH WHERE Exchange='Bittrex' ORDER BY Time desc";
							$coin_data_result = mysqli_query($conn, $coin_data_query);				
						?>

						var data = new google.visualization.DataTable();
						data.addColumn('datetime', 'Time');
						data.addColumn('number', 'Rate');
						
						var options = {
							title:'Bitcoin / Bitcoin Cash from Bittrex',
							displayZoomButtons:true,
							displayAnnotations: true,
							scaleType: 'allmaximized',
							legendPosition: 'newRow',
							thickness: 5,
							colors: ['#ffa500','#ffa500'],
							width:1000,
							height:400
						};
						
						data.addRows([
							  
							 <?php  
								
								$first_row = 1;
								$num_rows = mysqli_num_rows($coin_data_result);
				  
								while ($row = mysqli_fetch_assoc($coin_data_result)) {
									$time = $row['Time'];
									$price = $row['Rate'];
									
									if  ($first_row != $num_rows) {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price],";
									}
									else {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price]";
									}							
									
									$num_rows--;
								}
							?>		
				
						]);

						var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
						chart.draw(data, options);
					  }

					
					/**
					   * Draws the chart with coin/pair BTC/BCH from exchange Coinsquare
					   */
					  function drawChartBCH_Coinsquare() {

					  <?php						
							$coin_data_query = "SELECT * FROM BTC_BCH WHERE Exchange='Coinsquare' ORDER BY Time desc";
							$coin_data_result = mysqli_query($conn, $coin_data_query);				
						?>

						var data = new google.visualization.DataTable();
						data.addColumn('datetime', 'Time');
						data.addColumn('number', 'Rate');
						
						var options = {
							title:'Bitcoin / Bitcoin Cash from Coinsquare',
							displayZoomButtons:true,
							displayAnnotations: true,
							scaleType: 'allmaximized',
							legendPosition: 'newRow',
							thickness: 5,
							colors: ['#ffa500','#ffa500'],
							width:1000,
							height:400
						};
						
						data.addRows([
							  
							 <?php  
								
								$first_row = 1;
								$num_rows = mysqli_num_rows($coin_data_result);
				  
								while ($row = mysqli_fetch_assoc($coin_data_result)) {
									$time = $row['Time'];
									$price = $row['Rate'];
									
									if  ($first_row != $num_rows) {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price],";
									}
									else {
										echo "[new Date(";
										echo date("Y, m, d, H, i, s", strtotime($time . ' - 1 month')); 
										echo "), $price]";
									}							
									
									$num_rows--;
								}
							?>		
				
						]);

						var chart = new google.visualization.AreaChart(document.getElementById('chart_div2'));
						chart.draw(data, options);
					  }
					
					
					/**
					 * When you click BTC/ETH pill
					 */
					function ethClick(){
						console.log("ethClick");
						drawChartETH_Bittrex();
						drawChartETH_Coinsquare();						
					}

					/**
					 * When you click BTC/LTC pill
					 */
					function ltcClick(){
						console.log("ethClick");
						drawChartLTC_Bittrex();
						drawChartLTC_Coinsquare();
					}
	
					/**
					 * When you click BTC/BCH pill
					 */
					function bchClick(){
						console.log("bchClick");
						drawChartBCH_Bittrex();
						drawChartBCH_Coinsquare();						
					}

				</script>
				<div id='chart_div' height="1000" width="400"></div>
				<div id='chart_div2' height="1000" width="400"><div>
			</div>	
		
		</div>
		<br>
	  <br>
    <br>
	  <br>
	    <br>
	<!-- Light mode take out style="background-color:dimgray" -->
	<body class="text-center" style="background-color:white">	
			
			
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
		  
	</body>
</html>