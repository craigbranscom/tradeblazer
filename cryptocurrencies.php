<!doctype html>

<!-- 
	cryptocurrencies.php

	@author Vance Field
	@version 27-Feb-2018
-->
      
<?php
   include 'connection.php';

   session_start();
   $email = $_SESSION['email'];

?>

<html lang="en">

  <head>
  
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="icon" href="resources/CompassLogo.png">
	
    <title>Cryptocurrencies</title>

    <!-- Bootstrap core CSS -->	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">	
    <link href="https://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/cryptocurrencies.css" rel="stylesheet">
	
  </head>
	
	<!-- Implements the navbar and its components -->
	<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">
	<img src="resources/CompassLogo.png" width="20" height="20" class="d-inline-block align-top" alt="">
	TradeBlazer
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="dashboard.php">Dashboard<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="cryptocurrencies.php">Cryptocurrencies<span class="sr-only">(current)</span></a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="#">Trade Routes<span class="sr-only">(current)</span></a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="#">Walkthroughs<span class="sr-only">(current)</span></a>
      </li>
    </ul>	
	
	<ul class="navbar-nav">	
	
	<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Account
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="my-profile.php">My Profile</a>
        </div>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="sign-out-handler.php">Sign out<span class="sr-only">(current)</span></a>
      </li>
	 </ul>
  </div>
</nav>

<br>
<br>


        <main role="main" class="mr-sm-auto ml-sm-auto col-lg-11 pt-3 px-4">
		<br>		
		
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
			
				<!-- Dark mode <font color="white">Cryptocurrencies</font> -->
				<h1 class="h2"><font color="black">Cryptocurrencies</font></h1>
				
					<div class="btn-toolbar mb-2 mb-md-0">
						
						<div class="btn-group mr-2">
							<button class="btn btn-sm btn-warning">Favorite</button>
						</div>
						
						<div class="btn-group mr-2">
							<button class="btn btn-sm btn-warning">Share</button>
						</div>
					  
					</div>
			</div>
		
		<br>
		<br>
		
			<div class="row">
	  
			<!-- Light mode <nav class="col-md-2 d-none d-md-block bg-light sidebar"> -->
			<!-- <nav class="col-md-2 d-none d-md-block bg-light sidebar"> -->
		  
				<div class="sidebar-sticky">				
				  
					<!-- list of crypto -->		
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
					
						<a class="nav-link active bg-light text-dark" id="v-pills-btc-tab" data-toggle="pill" href="#v-pills-btc" role="tab" aria-controls="v-pills-btc" aria-selected="true"><?php echo getBitcoinAltIcon(); ?> <br>Bitcoin<br><br></a>
						<a class="nav-link bg-light text-dark" id="v-pills-eth-tab" data-toggle="pill" href="#v-pills-eth" role="tab" aria-controls="v-pills-eth" aria-selected="false"><?php echo getEthereumAltIcon(); ?> <br>Ethereum<br><br></a>
						<a class="nav-link bg-light text-dark" id="v-pills-ltc-tab" data-toggle="pill" href="#v-pills-ltc" role="tab" aria-controls="v-pills-ltc" aria-selected="false"><?php echo getLitecoinAltIcon(); ?> <br>Litecoin<br><br></a>
						<a class="nav-link bg-light text-dark" id="v-pills-bch-tab" data-toggle="pill" href="#v-pills-bch" role="tab" aria-controls="v-pills-bch" aria-selected="false"><?php echo getBitcoinCashAltIcon(); ?> <br>Bitcoin Cash<br><br></a>
						
					</div>
				</div>
				
			<!-- </nav> -->
				
					<!-- main panel -->
					<div class="tab-content" id="v-pills-tabContent" >			
					
						<div class="tab-pane fade" id="v-pills-btc" role="tabpanel" aria-labelledby="v-pills-btc-tab"> 
							<?php echo coinSelected("Bitcoin"); ?>
						</div>
						
						<div class="tab-pane fade" id="v-pills-eth" role="tabpanel" aria-labelledby="v-pills-eth-tab"> 
							<?php echo coinSelected("Ethereum"); ?> 
						</div>
						
						<div class="tab-pane fade" id="v-pills-ltc" role="tabpanel" aria-labelledby="v-pills-ltc-tab"> 
							<?php echo coinSelected("Litecoin"); ?>
						</div>
						
						<div class="tab-pane fade" id="v-pills-bch" role="tabpanel" aria-labelledby="v-pills-bch-tab">
							<?php echo coinSelected("Bitcoin Cash"); ?> 						 
						</div>
						
					</div>		
					
			</div>
		
	<!-- Dark mode style="background-color:dimgray" -->
	<body class="text-center">	
		


	<!-- php functions -->
	<?php 
	
		/**
		 * Controller for selecting coins.
		 */
		function coinSelected( $coin ){
			// which list item is selected?
			if ( $coin === "Bitcoin" ) { 
				echo getBitcoinIcon();
				echo "<br>";
				echo getBitcoinTitle();
				echo "<br>";
				echo getBitcoinBody();
				}
			else if ( $coin === "Ethereum" ) { 
				echo getEthereumIcon(); 
				echo "<br>";
				echo getEthereumTitle();
				echo "<br>";
				echo getEthereumBody();
				}
			else if ( $coin === "Litecoin" ) { 
				echo getLitecoinIcon();
				echo "<br>";
				echo getLitecoinTitle();
				echo "<br>";
				echo getLitecoinBody();
				}
			else if ( $coin === "Bitcoin Cash" ) { 
				echo getBitcoinCashIcon(); 
				echo "<br>";
				echo getBitcoinCashTitle();
				echo "<br>";
				echo getBitcoinCashBody();
				}
		}
		
			
		
		/**
		 * Returns the bitcoin icon.
		 */
		function getBitcoinIcon(){
			return '<img src="resources/BTC.png" height="200" width="200">';
		}
		
		/**
		 * Returns the bitcoin alt icon.
		 */
		function getBitcoinAltIcon(){
			return '<img src="resources/BTC-alt.png" height="40" width="40">';
		}
		
		/**
		 * Returns the Bitcoin title
		 */
		function getBitcoinTitle(){
			return "Bitcoin (BTC)";
		}
		
		/**
		 * Returns the Bitcoin body
		 */
		function getBitcoinBody(){
			return "Bitcoin (BTC) is the first decentralized digital currency. On August 18, 2008, 
					the domain name \"bitcoin.org\" was registered. Later that year a paper was released 
					online titled <i>Bitcoin: A Peer-to-Peer Electronic Cash System</i>, authored by 
					Satoshi Nakamoto. Nakamoto implemented the bitcoin software as open source code and 
					released it early the following year. In January 2009, the bitcoin network came into 
					existence after Satoshi Nakamoto mined the first ever block on the chain, known as the 
					genesis block.";
		}
		
		/**
		 * Returns the ethereum icon.
		 */
		function getEthereumIcon(){
			return '<img src="resources/ETH.png" height="200" width="200">';
		}
		
		/**
		 * Returns the ethereum alt icon.
		 */
		function getEthereumAltIcon(){
			return '<img src="resources/ETH-alt.png" height="40" width="40">';
		}
		
		/**
		 * Returns the ethereum title
		 */
		function getEthereumTitle(){
			return "Ethereum (ETH)";
		}
		
		/**
		 * Returns the ethereum body
		 */
		function getEthereumBody(){
			return "Ethereum is an open-source, public, blockchain-based distributed computing platform and 
					operating system featuring smart contract functionality. Ethereum was initially described 
					in a white paper by Vitalik Buterin, a programmer involved with Bitcoin Magazine, 
					in late 2013 with a goal of building decentralized applications. Buterin had argued that 
					Bitcoin needed a scripting language for application development. Failing to gain agreement, 
					he proposed development of a new platform with a more general scripting language.";
		}
		
		/**
		 * Returns the litecoin icon.
		 */
		function getLitecoinIcon(){
			return '<img src="resources/LTC.png" height="200" width="200">';
		}
		
		/**
		 * Returns the litecoin alt icon.
		 */
		function getLitecoinAltIcon(){
			return '<img src="resources/LTC-alt.png" height="40" width="40">';
		}
		
		/**
		 * Returns the litecoin title
		 */
		function getLitecoinTitle(){
			return "Litecoin (LTC)";
		}
		
		/**
		 * Returns the Litecoin body
		 */
		function getLitecoinBody(){
			return "Litecoin is a peer-to-peer cryptocurrency and open source software project released under the MIT/X11 license. 
					Litecoin was released via an open-source client on GitHub on October 7, 2011 by Charlie Lee, 
					a former Google employee. The Litecoin network went live on October 13, 2011. 
					It was a fork of the Bitcoin Core client, differing primarily by having a decreased block generation time, 
					increased maximum number of coins, different hashing algorithm, and a slightly modified GUI. 
					The coin was inspired by, and in technical details is nearly identical to, Bitcoin (BTC).";
		}
		
		/**
		 * Returns the Bitcoin Cash icon.
		 */
		function getBitcoinCashIcon(){
			return '<img src="resources/BCH.png" height="200" width="200">';
		}
		
		/**
		 * Returns the Bitcoin Cash alt icon.
		 */
		function getBitcoinCashAltIcon(){
			return '<img src="resources/BCH-alt.png" height="40" width="40">';
		}
		
		/**
		 * Returns the Bitcoin Cash title
		 */
		function getBitcoinCashTitle(){
			return "Bitcoin Cash (BCH)";
		}
		
		/**
		 * Returns the Bitcoin Cash body
		 */
		function getBitcoinCashBody(){
			return "Bitcoin Cash is a  is a hard fork of the cryptocurrency bitcoin. The bitcoin scalability debate led to the hard fork 
					on August 1, 2017, which resulted in the creation of a new blockchain. The first implementation of the Bitcoin Cash protocol 
					called Bitcoin ABC was revealed by Amaury \"Deadal Nix\" Séchet at the Future of Bitcoin conference in Arnhem, Netherlands.
					Upon launch, Bitcoin Cash inherited the transaction history of the bitcoin cryptocurrency on that date, but all later transactions were separate. ";
		}
	
	?>

	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
		  
	</body>
</html>