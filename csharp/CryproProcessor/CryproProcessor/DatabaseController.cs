﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryproProcessor
{

   /**
    * Database Helper
    * Contains all functions to transact with the database
    * @author Vance Field
    * @version 22-Feb-2018
    */ 
    public class DatabaseController 
    {
        // database controller object
        internal static object dbController;

        // mysql connection object
        MySqlConnection conn = null;
         

        /**
         * Initiates a connection to the MySql database
         */ 
        public void InitConnection()
        {
            // init connection to the database
            MySqlConnectionStringBuilder builder = new MySqlConnectionStringBuilder();
            builder.Server = "24.149.125.115";
            builder.Port = 3306;
            builder.Database = "tradeblazer";
            builder.UserID = "developer";
            builder.Password = "Fireb@ll99703";            

            String connectionStr = builder.ToString();

            builder = null;

            Console.WriteLine(connectionStr);

            try
            {
                conn = new MySqlConnection(connectionStr);
                Console.WriteLine("MySqlConnection created.");
            }
            catch(Exception e)
            {
                Console.WriteLine("MySqlConnection not created.");
                Console.WriteLine(e.Message);
            }


        }

        /**
         * Select all rows from table BTC-ETH
         */ 
        public void SelectAllFromBTC_ETH()
        {
            String query = "SELECT * FROM BTC_ETH;";

            MySqlCommand cmd = new MySqlCommand(query, conn);

            conn.Open();

            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                String exchange = (String) reader["Exchange"];
                //DateTime dts = (DateTime) reader["Timestamp"];
                decimal rate = (decimal) reader["Rate"];
                decimal volume = (decimal) reader["Volume"];

                Console.WriteLine("Exchange: " + exchange);
                //Console.WriteLine("Timestamp: " + timestamp);
                Console.WriteLine("Rate: " + rate);
                Console.WriteLine("Volume: " + volume + "\n");

            }
            conn.Close();
        }


        /**
         * Inserts the given coinPair into the given `table`
         * @param table    : the database table
         * @param exchange : the exchange the coin/pair is pulled from
         * @param rate     : the coin/pair rate from the given `exchange`
         * @param volume   : the coin/pair volume from the given `exchange`
         */
        public void InsertCoinPair(string table, string exchange, decimal rate, decimal volume)
        {
            String query = String.Format("insert into {0} (Exchange, Time, Rate, Volume)  values ('{1}', NOW(), '{2}', '{3}')", table, exchange , rate, volume );

            MySqlCommand cmd = new MySqlCommand(query, conn);

            conn.Open();

            try
            {
                cmd.ExecuteNonQuery();
                Console.WriteLine(DateTime.Now + " - Insert successful.");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(DateTime.Now + " -Insert unsuccessful.");
            }

            conn.Close();
        }
    }
}
