﻿using System;
using System.Timers;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Net;

namespace CryproProcessor
{
    /**
     * Prices - Pulls price data of currencies 
     * @author Vance Field
     * @version 22-Feb-2018
     */ 
    public class Prices
    {
        // databasecontroller object
        public static DatabaseController dbController = new DatabaseController();

        // httpclient 
        private static HttpClient client = new HttpClient();

        // httpresponse
        private static HttpResponseMessage responsePoloniex;
        private static HttpResponseMessage responseBittrex;
        private static HttpResponseMessage responseCoinsquare;

        // API links for coins
        private const string URL_POLONIEX = "https://poloniex.com/public?command=returnTicker";
        private const string URL_BITTREX = "https://bittrex.com/api/v1.1/public/getmarketsummaries";
        private const string URL_COINSQUARE = "https://coinsquare.io/api/v1/data/quotes";

        // timer time interval
        private const double TIMER_INTERVAL = 60000;


        /**
         * Constructor
         */
        public Prices()
        {
            // required for httpclient pulling from poloniex
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            // create database connection
            dbController.InitConnection();
        }


        /**
         * Starts `TIMER_INTERVAL` timer
         */
        public void StartTimer()
        {
            Console.WriteLine("Starting timer.\n");
            Timer timer = new Timer();
            timer.Interval = TIMER_INTERVAL;
            timer.Elapsed += new ElapsedEventHandler(Timer_ElapsedAsync);
            timer.Start();
            Console.ReadLine();
        }

        /**
         * Timer event. 
         * Fires every `TIMER_INTERVAL` (60ms).
         * Currently outputs coin data.
         */ 
        private static async void Timer_ElapsedAsync(object sender, ElapsedEventArgs e)
        {
            // pull from exchanges
            responsePoloniex = await client.GetAsync(URL_POLONIEX);            
            responseBittrex = await client.GetAsync(URL_BITTREX);
            responseCoinsquare = await client.GetAsync(URL_COINSQUARE);
            responsePoloniex.EnsureSuccessStatusCode();
            responseBittrex.EnsureSuccessStatusCode();
            responseCoinsquare.EnsureSuccessStatusCode();

            // parse json into string
            string contentPoloniex = await responsePoloniex.Content.ReadAsStringAsync();
            string contentBittrex = await responseBittrex.Content.ReadAsStringAsync();
            string contentCoinsquare = await responseCoinsquare.Content.ReadAsStringAsync();

            // insert data into database...

            // insert btc-eth from all exchanges
            dbController.InsertCoinPair("BTC_ETH", "Poloniex", GetPoloniexCoinPairLast(contentPoloniex, "BTC_ETH"), GetPoloniexCoinPairVolume(contentPoloniex, "BTC_ETH"));
            dbController.InsertCoinPair("BTC_ETH", "Bittrex", GetBitTrexCoinPairLast(contentBittrex, "BTC-ETH"), GetBitTrexCoinPairVolume(contentBittrex, "BTC-ETH"));
            dbController.InsertCoinPair("BTC_ETH", "CoinSquare", GetCoinSquareCoinPairLast(contentCoinsquare, "ETH"), GetCoinSquareCoinPairVolume(contentCoinsquare, "ETH"));

            // insert btc-ltc from all exchanges
            dbController.InsertCoinPair("BTC_LTC", "Poloniex", GetPoloniexCoinPairLast(contentPoloniex, "BTC_LTC"), GetPoloniexCoinPairVolume(contentPoloniex, "BTC_LTC"));
            dbController.InsertCoinPair("BTC_LTC", "Bittrex", GetBitTrexCoinPairLast(contentBittrex, "BTC-LTC"), GetBitTrexCoinPairVolume(contentBittrex, "BTC-LTC"));
            dbController.InsertCoinPair("BTC_LTC", "CoinSquare", GetCoinSquareCoinPairLast(contentCoinsquare, "LTC"), GetCoinSquareCoinPairVolume(contentCoinsquare, "LTC"));

            // insert btc-bch from all exchanges
            dbController.InsertCoinPair("BTC_BCH", "Poloniex", GetPoloniexCoinPairLast(contentPoloniex, "BTC_BCH"), GetPoloniexCoinPairVolume(contentPoloniex, "BTC_BCH") );
            dbController.InsertCoinPair("BTC_BCH", "Bittrex", GetBitTrexCoinPairLast(contentBittrex, "BTC-BCC"), GetBitTrexCoinPairVolume(contentBittrex, "BTC-BCC"));
            dbController.InsertCoinPair("BTC_BCH", "CoinSquare", GetCoinSquareCoinPairLast(contentCoinsquare, "BCH"), GetCoinSquareCoinPairVolume(contentCoinsquare, "BCH"));
            

            // get BTC_ETH last from exchanges
            /*
            // get BTC_ETH last from poloniex           
            Console.WriteLine("Poloniex BTC_ETH last: \t\t" + GetPoloniexCoinPairLast(contentPoloniex, "BTC_ETH"));
            // get BTC_ETH last from bittrex            
            Console.WriteLine("BitTrex BTC_ETH last: \t\t" + GetBitTrexCoinPairLast(contentBittrex, "BTC-ETH"));
            // get BTC_ETH last from coinsquare
            Console.WriteLine("CoinSquare BTC_ETH last: \t" + GetCoinSquareCoinPairLast(contentCoinsquare, "ETH"));
            Console.WriteLine();

            // get BTC_ETH volume from exchanges

            // get BTC_ETH volume from poloniex  
            Console.WriteLine("Poloniex BTC_ETH volume: \t" + GetPoloniexCoinPairVolume(contentPoloniex, "BTC_ETH"));
            // get BTC_ETH volume from bittrex
            Console.WriteLine("BitTrex BTC_ETH volume: \t" + GetBitTrexCoinPairVolume(contentBittrex, "BTC-ETH"));
            // get BTC_ETH volume from coinsquare
            Console.WriteLine("CoinSquare BTC_ETH volume: \t" + GetCoinSquareCoinPairVolume(contentCoinsquare, "ETH"));
            Console.WriteLine();

            // get BTC_LTC last from exchanges

            // get BTC_LTC last from poloniex
            Console.WriteLine("Poloniex BTC_LTC last: \t\t" + GetPoloniexCoinPairLast(contentPoloniex, "BTC_LTC"));
            // get BTC_LTC last from bittrex            
            Console.WriteLine("BitTrex BTC_LTC last: \t\t" + GetBitTrexCoinPairLast(contentBittrex, "BTC-LTC"));
            // get BTC_LTC last from coinsquare
            Console.WriteLine("CoinSquare BTC_LTC last: \t" + GetCoinSquareCoinPairLast(contentCoinsquare, "LTC"));
            Console.WriteLine();

            // get BTC_LTC volume from exchanges

            // get BTC_LTC volume from poloniex
            Console.WriteLine("Poloniex BTC_LTC volume: \t" + GetPoloniexCoinPairVolume(contentPoloniex, "BTC_LTC"));
            // get BTC_LTC volume from bittrex
            Console.WriteLine("BitTrex BTC_LTC volume: \t" + GetBitTrexCoinPairVolume(contentBittrex, "BTC-LTC"));
            // get BTC_LTC volume from coinsquare
            Console.WriteLine("CoinSquare BTC_LTC volume: \t" + GetCoinSquareCoinPairVolume(contentCoinsquare, "LTC"));
            Console.WriteLine();

            // get BTC_BCH last from exchanges

            // get BTC_BCH (Bitcoin cash) last from poloniex
            Console.WriteLine("Poloniex BTC_BCH last: \t\t" + GetPoloniexCoinPairLast(contentPoloniex, "BTC_BCH"));
            // get BTC-BCH (Bitcoin cash) last from bittrex
            Console.WriteLine("BitTrex BTC_BCH last: \t\t" + GetBitTrexCoinPairLast(contentBittrex, "BTC-BCC"));
            // get BTC-BCH (Bitcoin cash) last from coinsquare
            Console.WriteLine("CoinSquare BTC_BCH last: \t" + GetCoinSquareCoinPairLast(contentCoinsquare, "BCH"));
            Console.WriteLine();

            // get BTC_BCH (Bitcoin cash) volume from poloniex
            Console.WriteLine("Poloniex BTC_BCH volume: \t" + GetPoloniexCoinPairVolume(contentPoloniex, "BTC_BCH"));
            // get BTC-BCH (Bitcoin cash) volume from bittrex
            Console.WriteLine("BitTrex BTC_BCH volume: \t" + GetBitTrexCoinPairVolume(contentBittrex, "BTC-BCC"));
            // get BTC-BCH (Bitcoin cash) volume from coinsquare
            Console.WriteLine("CoinSquare BTC_BCH volume: \t" + GetCoinSquareCoinPairVolume(contentCoinsquare, "BCH"));
            Console.WriteLine();                     
            */
                     


        }


        /**
         * Task to parse the given Poloniex `coinPair` and return its Last price
         * @param content  : the json string
         * @param coinPair : the coin pair to retrieve 
         */
        private static decimal GetPoloniexCoinPairLast(string content, string coinPair)
        {
            // parse here
            JObject obj = JObject.Parse(content);
            return Decimal.Parse(obj.SelectToken(coinPair).SelectToken("last").ToString());
            
        }

        /**
         * Task to parse the given Poloniex `coinPair` and return its Volume
         * @param content  : the json string
         * @param coinPair : the coin pair to retrieve 
         */
        private static decimal GetPoloniexCoinPairVolume(string content, string coinPair)
        {
            // parse here
            JObject obj = JObject.Parse(content);
            return Decimal.Parse(obj.SelectToken(coinPair).SelectToken("baseVolume").ToString());
        }

        /**
         * Task to parse the given BitTrex `coinPair` and return its Last price
         * @param content  : the json string
         * @param coinPair : the coin pair to retrieve 
         */
        private static decimal GetBitTrexCoinPairLast(string content, string coinPair)
        {           

            // parse here
            JObject obj = JObject.Parse(content);
            JArray arr = (JArray)obj.SelectToken("result");
            JObject coinDetails = new JObject();

            // loop through the BitTrex results array
            foreach (var coin in arr)
            {
                // find the coinPair
                if (coin.SelectToken("MarketName").ToString().Equals(coinPair))
                {
                    // initialize a new JObject as the coinPair
                    coinDetails = (JObject) coin;
                    break;
                }
            }

            return Decimal.Parse(coinDetails.SelectToken("Last").ToString());            
        }

        /**
         * Task to parse the given BitTrex `coinPair` and return its volume
         * @param content  : the json string
         * @param coinPair : the coin pair to retrieve 
         */
        private static decimal GetBitTrexCoinPairVolume(string content, string coinPair)
        {

            // parse here
            JObject obj = JObject.Parse(content);
            JArray arr = (JArray)obj.SelectToken("result");
            JObject coinDetails = new JObject();

            // loop through the BitTrex results array
            foreach (var coin in arr)
            {
                // find the coinPair
                if (coin.SelectToken("MarketName").ToString().Equals(coinPair))
                {
                    // initialize a new JObject as the coinPair
                    coinDetails = (JObject)coin;
                    break;
                }
            }

            return Decimal.Parse(coinDetails.SelectToken("Volume").ToString());
        }

        /**
         * Task to parse the given CoinSquare `coinPair` and return its Last price
         * @param content  : the json string
         * @param ticker   : the coin pair to retrieve 
         */
        private static decimal GetCoinSquareCoinPairLast(string content, string ticker)
        {

            // parse here
            JObject obj = JObject.Parse(content);
            JArray arr = (JArray)obj.SelectToken("quotes");
            JObject coinDetails = new JObject();

            // loop through the CoinSquare results array
            foreach (var coin in arr)
            {
                // find the coinPair
                if (coin.SelectToken("ticker").ToString().Equals(ticker))
                {
                    // initialize a new JObject as the coinPair
                    coinDetails = (JObject)coin;
                    break;
                }
            }

            // must convert to double to fix decimal notation
            return Decimal.Parse((Convert.ToDecimal(coinDetails.SelectToken("last").ToString()) / 100000000).ToString());
            
        }

        /**
         * Task to parse the given CoinSquare `coinPair` and return its volume
         * @param content  : the json string
         * @param ticker   : the coin pair to retrieve 
         */
        private static decimal GetCoinSquareCoinPairVolume(string content, string ticker)
        {

            // parse here
            JObject obj = JObject.Parse(content);
            JArray arr = (JArray)obj.SelectToken("quotes");
            JObject coinDetails = new JObject();

            // loop through the CoinSquare results array
            foreach (var coin in arr)
            {
                // find the coinPair
                if (coin.SelectToken("ticker").ToString().Equals(ticker))
                {
                    // initialize a new JObject as the coinPair
                    coinDetails = (JObject)coin;
                    break;
                }
            }

            // must convert to double to fix decimal notation           
            return (Decimal.Parse(coinDetails.SelectToken("volume").ToString()) / 100000000);
        }

    }
}

