<!doctype html>

<!-- 
	my-profile.php

	@author Khalil Butler
	@author Cody French
	@version 27-Feb-2018
-->
      
<?php

   include 'connection.php';

   session_start();
   $email = $_SESSION['email'];
   
   $favoritesQuery = "SELECT Name From Favorites WHERE Email='$email'";

   $favoritesQueryResult = mysqli_query($conn, $favoritesQuery);
?>

<html lang="en">
  <head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="icon" href="resources/CompassLogo.png">
	
    <title>Profile</title>

    <!-- Bootstrap core CSS -->
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
	
    <link href="https://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
	
	<!-- Implements the navbar and its components -->
	<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">
	<img src="resources/CompassLogo.png" width="20" height="20" class="d-inline-block align-top" alt="">
	TradeBlazer
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="dashboard.php">Dashboard<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="cryptocurrencies.php">Cryptocurrencies<span class="sr-only">(current)</span></a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="#">Trade Routes<span class="sr-only">(current)</span></a>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="#">Walkthroughs<span class="sr-only">(current)</span></a>
      </li>
    </ul>
	<ul class="navbar-nav">
	<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Account
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="my-profile.php">My Profile</a>
        </div>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="sign-out-handler.php">Sign out<span class="sr-only">(current)</span></a>
      </li>
	 </ul>
  </div>
</nav>
<br>
<br>
<!-- Implements Sidebar and its components -->
<div class="container-fluid">
      <div class="row">
	  <!-- Dark mode <nav class="col-sm-3 d-none d-md-block bg-dark sidebar"> -->
	  <nav class="col-sm-3 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <ul class="nav flex-column">
			<br>
              <li class="nav-item">
                <a class="nav-link active" href="my-profile.php">
				<!-- Dark mode <font color="white">My Profile</font> -->
				<img src="resources/icons/user.png" width="200px" height="200px">
				<br>
				<br>
                  <font color="black">My Profile<span class="sr-only">(current)</span></font>
                </a>
              </li>
			  <br>
              <li class="nav-item">
                <a class="nav-link" href="#">
				<!-- Dark mode <font color="white">some text</font> -->
                  <font color="black">About Me<span class="sr-only">(current)</span></font>
                </a>
              </li>
            </ul>
          </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-9 pt-3 px-4">
		<br>
          <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3">
			<!-- Dark mode <font color="white">Favorites</font> -->
			<h1 class="h2"><font color="black">Favorites</font></h1>
            <div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-warning">Share</button>
              </div>
            </div>
          </div>
		<div class="align-items-left">
		
		<div id='chart_div' style='width: 1000px; height: 600px;'>
          <div class="table-responsive">
		  <!-- Dark mode style="color: white;" -->
            <table class="table table-striped table-sm" style="color: black;">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Cryptocurrency</th>
                </tr>
              </thead>
              <tbody>
                 
                  <?php
                     $counter = 0;
                     $num_rows = mysqli_num_rows($favoritesQueryResult);
  
                     while ($row = mysqli_fetch_assoc($favoritesQueryResult)) {
                        $counter++;
                        $name = $row['Name'];
                        
                        echo "<tr>";
                        echo "<td>$counter</td>";
                        echo "<td>$name</td>";
                        echo "</tr>";
                     }
                  ?>
                  
		 </tbody>
            </table>
          </div>
		</div>
	  </div>
	<!-- Dark mode style="background-color:dimgray" -->
	<body class="text-center">	
			
			
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
		  
	</body>
</html>