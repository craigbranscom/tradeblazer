<!doctype html>

<!-- 
	coin-detail.php

	@author Craig Branscom
	@author Vance Field
	@author Khalil Butler
	@version 26-Feb-2018
-->

<?php
	include 'connection.php';
	
	
	$coin_data_query = "SELECT * FROM BTC_ETH WHERE Exchange='Poloniex' ORDER BY Time desc";
	$coin_data_result = mysqli_query($conn, $coin_data_query);
?>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="icon" href="resources/CompassLogo.png">
	
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
	
    <title>Cryptocurrencies</title>
	
	<link href="https://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
	
	<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
    <script type='text/javascript'>
	
      google.charts.load('current', {'packages':['annotatedtimeline']});
      google.charts.setOnLoadCallback(drawChart);
	  
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('datetime', 'Time');
        data.addColumn('number', 'Rate');
		
		var options = {
          displayAnnotations: true,
		  scaleType: 'allmaximized',
		  legendPosition: 'newRow',
		  thickness: 3
        };
		
        data.addRows([
			  
			 <?php  
  				$day = 0;
				$first_row = 1;
				$num_rows = mysqli_num_rows($coin_data_result);
  
				while ($row = mysqli_fetch_assoc($coin_data_result)) {
					$time = $row['Time'];
					$price = $row['Rate'];
					
					if  ($first_row != $num_rows) {
						echo "[new Date(";
						echo date("Y, n, j, G, i, s", strtotime($time)); 
						echo "), $price],";
					}
					else {
						echo "[new Date(";
						echo date("Y, n, j, G, i, s", strtotime($time)); 
						echo "), $price]";;
					}					
					
					$num_rows--;
				}
			?>
				
          //[new Date(2008, 1 ,1), 30000],
          //[new Date(2008, 1 ,2), 14045],
          //[new Date(2008, 1 ,3), 55022],
          //[new Date(2008, 1 ,4), 75284],
          //[new Date(2008, 1 ,5), 41476],
          //[new Date(2008, 1 ,6), 33322]
        ]);

        var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
	
  </head>
  <!-- Dark mode style="background-color:dimgray" -->
  <body>
	
	<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">
	<img src="resources/CompassLogo.png" width="20" height="20" class="d-inline-block align-top" alt="">
	TradeBlazer
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Dashboard</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Cryptocurrencies
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">What's Hot?</a>
          <a class="dropdown-item" href="#">Top Cryptocurrencies</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">All Cryptocurrencies</a>
        </div>
      </li>
    </ul>
	<ul class="navbar-nav">
	<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Account
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">My Profile</a>
          <a class="dropdown-item" href="#">Forums</a>
        </div>
      </li>
	  <li class="nav-item">
        <a class="nav-link" href="#">Sign out<span class="sr-only">(current)</span></a>
      </li>
	 </ul>
  </div>
</nav>

	<!-- Dark mode <font color="white">some text</font> -->
	<h1 class="h3 mb-3 font-weight-normal"><center>BTC/ETH Comparison</h1>
	<div id='chart_div' style='width: 1100px; height: 400px;'></div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  
	<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  
  </body>
</html>